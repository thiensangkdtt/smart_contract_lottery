from brownie import Lottery, accounts, config, network, exceptions
from scripts.deploy import deploy_lottery
from web3 import Web3
from scripts.help_scripts import (
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
    get_account,
    fund_with_link,
    get_contract,
)
import pytest


def test_get_entrance_fee():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    # Arrange
    lottery = deploy_lottery()
    # Act
    # 4000 usd = 1 eth
    # 50 usd = x eth
    entrance_fee = lottery.getEntranceFee()
    expected_fee = Web3.toWei(0.0125, "ether")
    # Assert
    assert entrance_fee == expected_fee


# def test_cant_enter():
#     if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
#         pytest.skip()
#     # Arrange
#     lottery = deploy_lottery()
#     account = get_account()
#     # Act
#     # Assert
#     with pytest.raises(exceptions.VirtualMachineError):
#         lottery.enter({"from": get_account(), "value": lottery.getEntranceFee()})


def test_can_start_and_enter():
    # Arrange
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    account = get_account()
    lottery = deploy_lottery()
    # Act
    start_tx = lottery.startLottery({"from": account})
    start_tx.wait(1)
    enter_tx = lottery.enter({"from": account, "value": lottery.getEntranceFee()})
    enter_tx.wait(1)
    # Assert
    assert account == lottery.players(0)


def test_can_end_lottery():
    # Arrange
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    account = get_account()
    lottery = deploy_lottery()
    # Act
    start_tx = lottery.startLottery({"from": account})
    start_tx.wait(1)
    enter_tx = lottery.enter({"from": account, "value": lottery.getEntranceFee()})
    enter_tx.wait(1)
    fund_with_link(lottery)
    end_tx = lottery.endLottery({"from": account})
    end_tx.wait(1)

    # Assert
    assert lottery.lottery_state() == 2


def test_can_pick_winner_correctly():
    # Arrange
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()
    account = get_account()
    lottery = deploy_lottery()
    # Act
    start_tx = lottery.startLottery({"from": account})
    start_tx.wait(1)
    lottery.enter({"from": account, "value": lottery.getEntranceFee()})
    lottery.enter({"from": get_account(index=1), "value": lottery.getEntranceFee()})
    lottery.enter({"from": get_account(index=2), "value": lottery.getEntranceFee()})
    fund_with_link(lottery)
    # Ta sẽ test ở môi trường local, sử dụng mock để handle chainlink, khi
    # chainlink xử lý xong sẽ gọi hàm callBackWithRandomness => Ta sẽ giả sử
    # gọi hàm này và truyền số ngẫu nhiên là 777
    # In this transaction variable, it stores an attribute called events which stores all of our events
    transaction = lottery.endLottery({"from": account})
    request_id = transaction.events["RequestRandomness"]["requestId"]
    STATIC_RDN = 777
    get_contract("vrf_coordinator").callBackWithRandomness(
        request_id, STATIC_RDN, lottery.address
    )
    starting_balance_of_account = account.balance()
    balance_of_lottery = lottery.balance()
    # 777 % 3 = 0
    assert lottery.reccentWinners() == account
    assert lottery.balance() == 0
    assert account.balance() == starting_balance_of_account + balance_of_lottery
